## Table of contents

- Run REST API service
- Run CLI for the service

## Run REST API service on http://localhost:10000

- go to the restapi dir
``` $ cd restapi ```

- run the REST API service
``` $ go run main.go ```

## Run CLI for the service

- go to the restcli dir
``` $ cd restcli ```

- build CLI executable
``` $ go build clirest.go ```

- run CLI with successful validate
``` $ ./clirest -method POST -endpoint /validate -expression "What is 5 plus 2 divided by 2 multiplied by 6 plus 9?" ```

- run CLI with successful evaluate
``` $ ./clirest -method POST -endpoint /evaluate -expression "What is 5 plus 2 divided by 2 multiplied by 6 plus 9?" ```

- run CLI with unsuccessful evaluate
``` $ ./clirest -method POST -endpoint /evaluate -expression "What is 5 plus 2 divided by 2 multiplied by plus 9?"```

- run CLI with unsuccessful validate
``` $ ./clirest -method POST -endpoint /validate -expression "What is 5 plus 2 divided by 2 multiplied by plus 9?"```

- run CLI with default parameters(GET /errors)
``` $ ./clirest ```

- run CLI get errors
``` $ ./clirest -method GET -endpoint /errors ```

- run CLI with different method 
``` $ ./clirest -method PUT -endpoint /errors ```


// main.go
package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
)

// Input - Our struct for input expression
type Input struct {
	Expression string `json:"expression"`
}

// Evaluate output
// {"result":<the expression's result>}
type Output struct {
	Result string `json:"result"`
}

// Input - Our struct for input expression
type InvalidOutput struct {
	Valid  bool    `json:"valid"`
	Reason *string `json:"reason"`
}

type ValidOutput struct {
	Valid bool `json:"valid"`
}

type Key struct {
	expression string
	endpoint   string
	errorType  string
}
type Error struct {
	key       Key
	frequency int
}

type ErrorOutput struct {
	Expression string `json:"expression"`
	Endpoint   string `json:"endpoint"`
	Frequency  int    `json:"frequency"`
	Type       string `json:"type"`
}

// var Errors []ErrorOutput
var ErrorsArray []Error

func returnAllErrors(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: returnAllErrors")
	errorMap := sumErrors()
	var Errors []ErrorOutput
	for key, value := range errorMap {
		errorNew := ErrorOutput{Expression: key.expression, Endpoint: key.endpoint, Frequency: value, Type: key.errorType}
		Errors = append(Errors, errorNew)
	}
	json.NewEncoder(w).Encode(Errors)
}

func evaluateExpression(w http.ResponseWriter, r *http.Request) {

	reqBody, _ := io.ReadAll(r.Body)
	var inputExpression Input
	json.Unmarshal(reqBody, &inputExpression)

	fmt.Println("Evaluate Endpoint Hit: " + inputExpression.Expression)

	v1, v2 := parseExpression(inputExpression.Expression, "/evaluate")

	var output Output
	if v1.Valid == true {
		output = Output{Result: strconv.Itoa(v2)}
	} else {
		output = Output{Result: "Error " + *v1.Reason}
	}
	json.NewEncoder(w).Encode(output)
}

func validateExpression(w http.ResponseWriter, r *http.Request) {

	reqBody, _ := io.ReadAll(r.Body)
	var inputExpression Input
	json.Unmarshal(reqBody, &inputExpression)

	fmt.Println("Validate Endpoint Hit: " + inputExpression.Expression)

	v1, _ := parseExpression(inputExpression.Expression, "/validate")
	if v1.Valid == true {
		json.NewEncoder(w).Encode(ValidOutput{Valid: true})
	} else {
		json.NewEncoder(w).Encode(v1)
	}
}

func parseExpression(expression string, endpoint string) (InvalidOutput, int) {
	errorSizeBegin := len(ErrorsArray)
	result := -1
	res1 := strings.HasPrefix(expression, "What is ")
	res2 := strings.HasSuffix(expression, "?")
	if res2 == true && res1 == false {
		createNewError(expression, endpoint, "Non-math question")
	}
	if res1 == true {
		inputFmt := expression[:len(expression)-1]
		inputFmt = inputFmt[len("What is "):]
		inputFmt = strings.TrimSpace(inputFmt)

		result = calculate(inputFmt, expression, endpoint)
	}

	if len(ErrorsArray) != errorSizeBegin {
		lastError := ErrorsArray[len(ErrorsArray)-1]
		return InvalidOutput{Valid: false, Reason: &lastError.key.errorType}, result
	} else {
		return InvalidOutput{Valid: true}, result
	}
}

func getNum(expression string) (int, string) {
	res := strings.LastIndex(expression, " ")

	if res > 0 {
		i, err := strconv.Atoi(strings.TrimSpace(expression[res:]))

		if err == nil {
			return i, expression[:res]
		} else {
			return i, "Unsupported operation"
		}
	} else {
		i, err := strconv.Atoi(strings.TrimSpace(expression))

		if err == nil {
			return i, "Final"
		}
	}
	return res, "Non-math question"
}

func getOperationString(expression string) (string, string) {
	m := regexp.MustCompile(`[0-9]`)
	index := m.FindAllStringIndex(expression, -1)

	if index[len(index)-1][1] > 0 {
		operation := expression[index[len(index)-1][1]:]
		return operation, expression[:index[len(index)-1][1]]
	} else {
		return expression, ""
	}
}

func calculate(express string, basicExpression string, endpoint string) int {

	second, express := getNum(express)
	fmt.Println("Express " + express)
	if express == "Unsupported operation" {
		createNewError(basicExpression, endpoint, express)
		return -222222
	}
	if express == "Final" {
		return second
	} else {
		op, express := getOperationString(express)
		fmt.Println("Operation " + op)
		first := calculate(express, basicExpression, endpoint)
		switch strings.TrimSpace(op) {
		case "plus":
			return first + second
		case "minus":
			return first - second
		case "multiplied by":
			return first * second
		case "divided by":
			return first / second
		default:
			createNewError(basicExpression, endpoint, "Expression with invalid syntax")
			return -111111
		}
	}
}

func createNewError(express string, endpoint string, errorType string) {
	errorNew := Error{key: Key{expression: express, endpoint: endpoint, errorType: errorType}, frequency: 1}
	ErrorsArray = append(ErrorsArray, errorNew)
}

func sumErrors() map[Key]int {
	// calculate sum:
	m := map[Key]int{}
	for _, v := range ErrorsArray {
		m[v.key] += v.frequency
	}
	return m
}

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/errors", returnAllErrors)
	myRouter.HandleFunc("/evaluate", evaluateExpression).Methods("POST")
	myRouter.HandleFunc("/validate", validateExpression).Methods("POST")
	log.Fatal(http.ListenAndServe(":10000", myRouter))
}

func main() {

	handleRequests()
}

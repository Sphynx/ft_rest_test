package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"net/http"
)

var baseUrl string = "http://localhost:10000"

type EvaluationRequest struct {
	Expression string `json:"expression"`
}

func main() {
	methodPtr := flag.String("method", "GET", "Http method {GET|POST}.")
	expressionPtr := flag.String("expression", "What is 5 plus 2?", "Mathematic question.")
	endpointPtr := flag.String("endpoint", "/errors", "HTTP endpoint.")
	flag.Parse()

	requestCall(*methodPtr, *endpointPtr, *expressionPtr)
}

func requestCall(method string, endpoint string, expression string) {

	switch method {
	case "GET":
		getRequest(endpoint)
	case "POST":
		postRequest(endpoint, expression)
	default:
		fmt.Println("Method is not supported")
	}

}

func postRequest(endpoint string, expression string) {
	// Create JSON payload
	payload := EvaluationRequest{
		Expression: expression,
	}

	// Marshal payload into JSON
	payloadBytes, err := json.Marshal(payload)
	if err != nil {
		fmt.Println("Error marshalling JSON:", err)
		return
	}

	// Make a POST request
	response, err := http.Post(baseUrl+endpoint, "application/json", bytes.NewBuffer(payloadBytes))
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	defer response.Body.Close()

	// Read the response body
	body, err := io.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error reading response:", err)
		return
	}

	// Print the response body
	fmt.Println(string(body))
}

func getRequest(endpoint string) {

	// Make a GET request
	response, err := http.Get(baseUrl + endpoint)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	defer response.Body.Close()

	// Read the response body
	body, err := io.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error reading response:", err)
		return
	}

	// Print the response body
	fmt.Println(string(body))
}
